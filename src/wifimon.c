/* wifimon.c : - Lucas McCoy - September 28th, 2017
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ncurses.h>
#include <menu.h>
#include <pcap.h>
#include "debug.h"
#include "wifimon.h"

WINDOW *draw_main_window();
WINDOW *win;

// FIXME: Yeah so this is bad but I'll think of something better later on.
static int line = 2;

void display_linux_mac(char *name)
{
	int fd;
	struct ifreq ifr;
	unsigned char *mac;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, name, IFNAMSIZ - 1);
	ioctl(fd, SIOCGIFHWADDR, &ifr);
	close(fd);
	mac = (unsigned char *)ifr.ifr_hwaddr.sa_data;
	mvwprintw(win, ++line, 5, "%s MAC: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x", name, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

void show_mac_addresses()
{
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *interfaces, *temp;

	// Find all devices, if an error occurs it will be saved in errbuf and we can print that out.
	if (pcap_findalldevs(&interfaces, errbuf) == -1) {
		fprintf(stderr, "Error in pcap findalldevs: %s\n", errbuf);
		return;
	}

	// Look through all the interface names that we get from pcap_findalldevs.
	for (temp = interfaces; temp; temp = temp->next) {
		// We've found an ethernet interface.
		if (strncmp("enp", temp->name, 3) == 0) {
			display_linux_mac(temp->name);
		}
		
		// We've found a wireless interface.
		if (strncmp("wlp", temp->name, 3) == 0) {
			display_linux_mac(temp->name);
		}
	}
}

static WINDOW *init_menubar(const enum wifimon_screen active)
{
	WINDOW *menu = newwin(1, WIDTH, 0, 0);
	enum wifimon_screen cur;

	nodelay(menu, true);
	keypad(menu, true);
	wmove(menu, 0, 0);
	for (cur = SCR_INFO; cur <= SCR_QUIT; cur++) {
		wattrset(menu, A_REVERSE | A_BOLD);
		wprintw(menu, "F%d", cur + 1);

		wattrset(menu, cur != active ? COLOR_PAIR(CP_INACTIVE)
					     : COLOR_PAIR(CP_ACTIVE) | A_BOLD);
		if (*screens[cur].key_name) {

			wattron(menu, A_UNDERLINE);
			waddch(menu, screens[cur].key_name[0]);
			wattroff(menu, A_UNDERLINE);

			wprintw(menu, "%-5s", screens[cur].key_name + 1);
		} else  {
			wprintw(menu, "%-6s", "");
		}
	}
	wrefresh(menu);

	return menu;
}

WINDOW *draw_main_window()
{
	WINDOW *win = newwin(HEIGHT, WIDTH - 1, 1, 1);
	return win;
}

int show_ip_address()
{
	char *device;
	char ip[20];
	char subnet_mask[20];
	bpf_u_int32 ip_raw;
	bpf_u_int32 subnet_mask_raw;
	int rc;
	char err_buf[PCAP_ERRBUF_SIZE];
	struct in_addr address;

	device = pcap_lookupdev(err_buf);
	check(device != NULL, "%s\n", err_buf);
	rc = pcap_lookupnet(device, &ip_raw, &subnet_mask_raw, err_buf);
	check(rc != -1, "%s\n", err_buf);

	address.s_addr = ip_raw;
	strcpy(ip, inet_ntoa(address));
	check(ip != NULL, "inet_ntoa");

	address.s_addr = subnet_mask_raw;
	strcpy(subnet_mask, inet_ntoa(address));
	check(subnet_mask != NULL, "inet_ntoa");


	mvwprintw(win, line, 5, "Device: %s", device);
	mvwprintw(win, ++line, 5, "IP Address: %s", ip);
	mvwprintw(win, ++line, 5, "Subnet Mask: %s", subnet_mask);

	return 0;
error:
	return 1;
}

int main(int argc, char *argv[])
{
	int ch;
	enum wifimon_screen curscr;
	curscr = SCR_INFO;

	initscr();
	clear();
	noecho();
	cbreak();
	keypad(stdscr, true);
	start_color();

	init_pair(CP_ACTIVE, COLOR_RED, COLOR_WHITE);
	init_pair(CP_INACTIVE, COLOR_RED, COLOR_BLACK);

	win = draw_main_window();
	refresh();
	show_ip_address();
	show_mac_addresses();
	box(win, ACS_VLINE, ACS_HLINE);
	mvwprintw(win, 0, 1, "Interfaces");
	touchwin(win);
	wrefresh(win);

	WINDOW *menu = init_menubar(SCR_INFO);
	wrefresh(menu);

	while ((ch = wgetch(menu)) != 'q') {
		switch (ch) {
			case KEY_LEFT:
				if (curscr > MIN) {
					menu = init_menubar(--curscr);
				}
				wrefresh(menu);
				break;
			case KEY_RIGHT:
				if (curscr < MAX) {
					menu = init_menubar(++curscr);
				}
				wrefresh(menu);
				break;
			case 10:
				if (curscr == SCR_QUIT) {
					cdebug("%d", curscr);
					endwin();
					return 0;
				}
			case KEY_F(10):
				endwin();
				return 0;
		}
	}

	endwin();
	return 0;
}
