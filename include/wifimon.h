#define WIDTH (COLS)
#define HEIGHT (LINES - 1)
#define MIN 0
#define MAX 9

enum color_pair {
	CP_STANDARD = 1,
	CP_SCALEHI,
	CP_SCALEMID,
	CP_SCALELOW,
	CP_WTITLE,
	CP_INACTIVE,
	CP_ACTIVE,
	CP_STATSIG,
	CP_STATNOISE,
	CP_STATSNR,
	CP_STATBKG,
	CP_STATSIG_S,
	CP_STATNOISE_S,
	CP_PREF_NORMAL,
	CP_PREF_SELECT,
	CP_PREF_ARROW,
	CP_SCAN_CRYPT,
	CP_SCAN_UNENC,
	CP_SCAN_NON_AP
};

enum wifimon_screen {
	SCR_INFO,	/* F1 */
	SCR_LHIST,	/* F2 */
	SCR_SCAN,	/* F3 */
	SCR_EMPTY_F4,	/* placeholder */
	SCR_EMPTY_F5,	/* placeholder */
	SCR_EMPTY_F6,	/* placeholder */
	SCR_PREFS,	/* F7 */
	SCR_HELP,	/* F8 */
	SCR_ABOUT,	/* F9 */
	SCR_QUIT	/* F10 */
};

static const struct {
	const char *const	key_name;
	void		 	(*init)(void);
	int			(*loop)(WINDOW *);
	void			(*fini)(void);
} screens[] = {
	[SCR_INFO]	= {
		.key_name = "File",
//		.init	  = scr_info_init,
//		.loop	  = scr_info_loop,
//		.fini	  = scr_info_fini
	},
	[SCR_LHIST]	= {
		.key_name = "Edit",
//		.init	  = scr_lhist_init,
//		.loop	  = scr_lhist_loop,
//		.fini	  = scr_lhist_fini
	},
	[SCR_SCAN]	= {
		.key_name = "View",
//		.init	  = scr_aplst_init,
//		.loop	  = scr_aplst_loop,
//		.fini	  = scr_aplst_fini
	},
	[SCR_EMPTY_F4]	= {
		.key_name = "Options",
	},
	[SCR_EMPTY_F5]	= {
		.key_name = "Testing Longer Names",
	},
	[SCR_EMPTY_F6]	= {
		.key_name = "",
	},
	[SCR_PREFS]	= {
		.key_name = "",
//		.init	  = scr_conf_init,
//		.loop	  = scr_conf_loop,
//		.fini	  = scr_conf_fini
	},
	[SCR_HELP]	= {
		.key_name = "",
//		.init	  = scr_help_init,
//		.loop	  = scr_help_loop,
//		.fini	  = scr_help_fini
	},
	[SCR_ABOUT]	= {
		.key_name = "",
//		.init	  = scr_about_init,
//		.loop	  = scr_about_loop,
//		.fini	  = scr_about_fini
	},
	[SCR_QUIT]	= {
		.key_name = "Exit",
	}
};

/*static char *screen_names[] = {
	[SCR_INFO]	= "Info screen",
	[SCR_LHIST]	= "Histogram",
	[SCR_SCAN]	= "Scan window",
	NULL
};*/

