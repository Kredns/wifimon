#CC=gcc
CFLAGS=-Iinclude/ -Wall -g -ggdb
DEPS=debug.h
LIBS=-lncurses -lpcap
DEBUG=-DNDEBUG
#SRC:=src
#wifimon: $(SRC)/*.c
#	$(CC) $(CFLAGS) $< -o $@ $(LIBS)
#
#clean:
#	rm -f main

SRC := src
OBJ := obj

SOURCES := $(wildcard $(SRC)/*.c)
OBJECTS := $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SOURCES))

wifimon: $(OBJECTS)
	$(CC) $(CFLAGS) $(DEBUG) $^ -o $@ $(LIBS)

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) $(DEBUG) -c $< -o $@

clean:
	rm -f obj/*.o
	rm -f wifimon
